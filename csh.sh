#!/bin/bash

if [ ! -d "config" ];then
mkdir ql/data/config
echo "创建config成功"
else
echo "文件夹config已经存在"
fi

if [ ! -d "db" ];then
mkdir ql/data/db
echo "创建db成功"
else
echo "文件夹db已经存在"
fi

if [ ! -d "deps" ];then
mkdir ql/data/deps
echo "创建deps成功"
else
echo "文件夹deps已经存在"
fi

if [ ! -d "jbot" ];then
mkdir ql/data/jbot
echo "创建jbot成功"
else
echo "文件夹jbot已经存在"
fi
if [ ! -d "log" ];then
mkdir ql/data/log
echo "创建log成功"
else
echo "文件夹log已经存在"
fi
if [ ! -d "raw" ];then
mkdir ql/data/raw
echo "创建raw成功"
else
echo "文件夹raw已经存在"
fi
if [ ! -d "repo" ];then
mkdir ql/data/repo
echo "创建repo成功"
else
echo "文件夹repo已经存在"
fi
if [ ! -d "scripts" ];then
mkdir ql/data/scripts
echo "创建scripts成功"
else
echo "文件夹scripts已经存在"
fi
     # 授权
    chmod 755 config
    chmod 755 db
    chmod 755 deps
    chmod 755 jbot
    chmod 755 log
    chmod 755 raw
    chmod 755 repo
    chmod 755 scripts

    echo -e "\n配置到此结束"